﻿using UnityEngine;
using System.Collections;

public class CharacterPhysics : MonoBehaviour {

	//basic physics properties, all in units per second
	public float acceleration = 1f;
	public float maxSpeed = 37.5f;
	public float gravity = 1.5f;
	public float maxFall = 30f;
	public float jump = 40f;

	//a layer mask, that is set in the Start() function
	int layerMask;

	//a Rectangle class has some useful tools, that will help us with collision detection
	Rect hitBox;

	//the velocity that will be applied to our character
	Vector2 velocity;

	//variables for checking the state of our character
	bool grounded = false;
	bool falling = false;

	//variables for raycasting: how many rays should we use, etc.
	int horizontalRays = 4;
	int verticalRays = 6;
	int margin = 2;

	//Variables for jumping
	bool input = false;
	bool lastInput = false;
	float jumpPressedTime = 0f;
	public float jumpPressLeeway = 0.01f;

	// Use this for initialization
	void Start () {
		layerMask = LayerMask.NameToLayer("normalCollision");
	}
	
	// FixedUpdate is called once per frame
	void FixedUpdate () {
		//This line of code saves a ton of typing later on in this function
		hitBox = new Rect(
			collider.bounds.min.x,
			collider.bounds.min.y,
			collider.bounds.size.x,
			collider.bounds.size.y
		);

		//An elegant way to apply gravity. Subtract from y speed, with the terminal velocity set equal to maxFall.
		if(!grounded)
			velocity = new Vector2(velocity.x, Mathf.Max (velocity.y - gravity, -maxFall));

		if(velocity.y < 0){
			falling = true;
		}

		if(grounded || falling){ //don't check anything if I'm moving up in the air
			Vector3 startPoint = new Vector3(hitBox.xMin + margin, hitBox.center.y, transform.position.z);
			Vector3 endPoint = new Vector3(hitBox.xMax - margin, hitBox.center.y, transform.position.z);

			RaycastHit hitInfo;

			//add half my hitBox height since we're starting at the center
			float distance = hitBox.height / 2 + (grounded? margin : Mathf.Abs(velocity.y * Time.deltaTime));
			/*
			 * (variable? doThis : doThat) is basically a shortcut for the if statement. 
			 * The statement before the ? is the condition, The statement after the ? is what happens if the condition is true, 
			 * and the statement after the : is what happens if the condition is false.
			 * The official name of this shortcut is the "ternary operator", and I use it to determine how long the ray needs to be,
			 * based on whether the hitBox is grounded or not.
			 */

			//check if the hitBox hit anything during its flight. Starts false because if any ray connects, the hitBox is grounded.
			bool connected = false;

			for(int i = 0; i < verticalRays; i++){
				float lerpAmount = (float)i / (float)(verticalRays - 1f); //We have to subtract 1, otherwise lerpAmount will never reach the value 1.
				Vector3 origin = Vector3.Lerp(startPoint, endPoint, lerpAmount);
				Ray ray = new Ray(origin, Vector3.down);

				connected = Physics.Raycast(ray, out hitInfo, distance, layerMask);

				if(connected){
					grounded = true;
					falling = false;
					transform.Translate(Vector3.down * (hitInfo.distance - hitBox.height/2));
					velocity = new Vector2(velocity.x, 0);
					break;
				}
			}

			if(!connected){
				grounded = false;
			}

			//This is how our character jumps. In order to allow the character to jump right after landing, we must do a little bit of extra logic.
			input = Input.GetButton("Jump");
			if (input && !lastInput){
				jumpPressedTime = Time.time;
			}
			else if (!input){
				jumpPressedTime = 0;
			}
			
			if (grounded && Time.time - jumpPressedTime < jumpPressLeeway){
				grounded = false;
				velocity = new Vector2(velocity.x, jump);
				jumpPressedTime = 0;
			}
			
			lastInput = input;
		}

	}

	void LateUpdate () { //this method applies the velocity of our character
		transform.Translate(velocity * Time.deltaTime); //applies movement. Time.deltaTime is the time since the last frame.
	}
}
